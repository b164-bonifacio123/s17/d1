console.log("Hello World")

//Arrays and Indexes
//store multiple values in a single variable.
//[] ------> Array Literals
//{} ------> Object Literals


//Common Examples of Arrays

let grades = [ 98.5, 91.2, 93.1 , 89.0];
console.log(grades[0])

//Alternative way to write arrays
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake express js'
];

//Reassigning array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'hello world';
console.log('Array after reassignment');
console.log(myTasks);
//Accessing an array element that does not exist will return "undefined"
console.log(myTasks[4]);

//Getting the length of an array

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Toshiba', 'Gateway', 'Redfox', 'Fujitsu'];

console.log(computerBrands.length);
console.log(computerBrands)

if(computerBrands.length > 5) {
	console.log('We have too many suppliers. Please coordinate with the operations manager.')
}

//Access the last element of an array
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

//ARRAY METHODS

//Mutator Methods
	// are functions that 'mutate' or change an array after they're created.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];


//push() method
/*
Adds an element in the end of an array AND returns array's length
	syntax:
		arrayName.push();
*/

console.log('Current Array: ')
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);

//Add elements
fruits.push('Avocado', 'Guava')
console.log('Mutated array from push method');
console.log(fruits);


//pop()
/*
-Removes the last element in an array AND returns the removed element
Syntax:
	arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

//unshift() method
/*
- Adds one or more elements at the beginning of an array
Syntax:
	arrayName.unshift('elementA', 'elementB')
*/

fruits.unshift('Lime', 'Banana');

console.log('Mutated array from unshift method')
console.log(fruits);

//shift() method
/*
- Removes an element at the beginning of an array AND returns the removed element
Syntax:
	arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);


//splice()
/*
- Simultaneously removes elements from a specified index number AND adds elements
Syntax:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(3, 2, 'Lime', 'Cherry');
console.log('splice method: ');
console.log(fruits);


//sort()
/*
- Rearranges the array elements in alphanumeric order. 
Syntax:
	arrayName.sort();
*/

fruits.sort();
console.log('sort method');
console.log(fruits);

//reverse()
/*
- Reverse te order of array elements.
Syntax:
	arrayName.reverse()
*/

fruits.reverse()
console.log("reverse method");
console.log(fruits);


//Non-Mutator Methods






















